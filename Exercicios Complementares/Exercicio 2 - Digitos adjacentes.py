'''
EXEMPLO DADO PELO PROFESSOR

Digite um número inteiro: 12345
não

Digite um número inteiro: 1011
sim

'''

numeroRecebido = int(input("Digite um número inteiro: "))

condicaoDeParada = False

while(numeroRecebido > 0 and not condicaoDeParada):
    valorModificado = numeroRecebido // 10
    if(numeroRecebido % 10 == valorModificado % 10):
        condicaoDeParada = True
    else:
        numeroRecebido = numeroRecebido // 10

if(condicaoDeParada):
    print("sim")
else:
    print("não")
    

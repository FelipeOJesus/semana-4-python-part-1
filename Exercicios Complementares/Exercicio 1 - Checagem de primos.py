'''
EXEMPLO DADO PELO PROFESSOR

Digite um número inteiro: 13
primo

Digite um número inteiro: 12
não primo

'''

numeroInteiro = int(input("Digite um número inteiro: "))

numeroPrimo = False

i = 1
QuantidadeDeVezesDividida = 0

while (i <= numeroInteiro and numeroPrimo == False):
    if(numeroInteiro % i == 0):
        QuantidadeDeVezesDividida += 1
        if(QuantidadeDeVezesDividida > 2):
            numeroPrimo = True
    i+=1

if (numeroPrimo):
    print("não primo")
else:
    print("primo")

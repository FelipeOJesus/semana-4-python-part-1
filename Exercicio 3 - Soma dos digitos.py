'''
EXEMPLO DADO PELO PROFESSOR

Digite um número inteiro: 123
6

'''

numeroInteiro = int(input("Digite um número inteiro: "))

resultado = 0

while(numeroInteiro > 0):
    digitoASomar = numeroInteiro % 10
    resultado += digitoASomar
    numeroInteiro = numeroInteiro // 10

print(resultado)
